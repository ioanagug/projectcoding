public class Main {
    public static void main(String[] args) {
        Poem[] poezii = new Poem[3];
        Author autor1 = new Author("Eminescu", "roman");
        Author autor2 = new Author("Shakespeare", "englez");
        Author autor3 = new Author("Stanescu", "roman");
        poezii[0] = new Poem(autor1,100);
        poezii[1] = new Poem(autor2, 30);
        poezii[2] = new Poem(autor3, 2);
        int max=0;
        for (int i = 0; i < poezii.length; i++) {
            if (poezii[i].stropheNumbers > max) {
                max = poezii[i].stropheNumbers;
            }
        }
        System.out.println(max);

        String s= Author.prize;
        String nume= autor1.getSurname();
        autor1.prize="Grammy";
        System.out.println(autor2.prize);
    }
}

