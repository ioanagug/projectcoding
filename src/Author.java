public class Author {
    public static String prize = "Nobel";
    private String surname;
    String nationality;

    public String getSurname() {
        return surname;
    }

    public Author(String surname, String nationality) {
        this.surname = surname;
        this.nationality = nationality;
    }

    @Override
    public String toString() {
        return "Author{" +
                "surname='" + surname + '\'' +
                ", nationality='" + nationality + '\'' +
                '}';
    }
}
