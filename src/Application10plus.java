import java.util.Random;
import java.util.Scanner;

public class Application10plus {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Alegeti numarul problemei:");
        int task = input.nextInt();
        input.nextLine();
        switch (task) {
            case 10: {
                System.out.println("Intorduceti numarul dorit:");
                int nr = input.nextInt();
                sumaCifre(nr);
                break;
            }
            case 11: {
                enough();
                break;
            }
            case 12: {
                readSpace();
                break;
            }
            case 13: {
                String text = "Ana are mere";
                stutter(text);
                break;
            }
            case 14: {
                alphabet('a', 'h');
                break;
            }
            case 15: {
                duplicate();
                break;
            }
            case 16: {
                increasing();
                break;
            }
            case 18: {
                sneeze();
                break;
            }
            case 20: {
                tooMuch();
                break;
            }
            case 21: {
                System.out.println("Introduceti text ");

                String text = input.nextLine();
                reverse(text);
                break;
            }
        }

    }

    public static void sumaCifre(int a) {
        int suma = 0;
        int init = a;
        while (a > 0) {
            suma = suma + a % 10;
            a = a / 10;
        }
        System.out.println("Suma cifrelor lui " + init + " este " + suma);
    }

    public static void enough() {
        String[] sir = new String[10];
        Scanner input = new Scanner(System.in);
        System.out.println("Scrieti un cuvant:");
        String s = input.nextLine();
        boolean enough = s.equals("Enough!");
        int count = 0;
        sir[count] = s;
        count++;
        if (s.equals("")) {
            System.out.println("N-ati scris nimic :( ");
        } else {
            while (enough == false) {
                System.out.println("Scrieti alt cuvant:");
                s = input.nextLine();
                enough = s.equals("Enough!");
                sir[count] = s;
                count++;
            }
        }
        int max = 0;
        String cuvant = sir[0];
        for (int i = 0; i < sir.length; i++) {
            if (sir[i] != null) {
                boolean stop = sir[i].equals("Enough!");
                if (stop == false) {
                    if (sir[i].length() > max) {
                        max = sir[i].length();
                        cuvant = sir[i];
                    }
                }
            }
        }
        System.out.println("Cuvantul cel mai lung este " + cuvant + " si are " + max + " litere");
    }

    public static void readSpace() {
        Scanner input = new Scanner(System.in);
        System.out.println("Introduceti textul dorit: ");
        String s = input.nextLine();
        int count = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == ' ') {
                count++;
            }
        }
        float percent = (float) count * 100 / s.length();
        System.out.println("Caracterul spatiu apare in text in procent de " + percent + "%");
    }

    public static void stutter(String s) {
        String[] sir = s.split(" ", -1);
        for (int i = 0; i < sir.length; i++) {
            System.out.print(sir[i] + " " + sir[i] + " ");
        }
    }

    public static void alphabet(char a, char b) {
        int ascii1 = a;
        int ascii2 = b;
        int dif = Math.abs(ascii1 - ascii2) - 1;
        System.out.println("Intre literele " + a + " si " + b + " sunt alte " + dif + " litere in alfabet.");
    }

    public static void duplicate() {
        Scanner input = new Scanner(System.in);
        int[] sir = new int[10];
        for (int i = 0; i <= 9; i++) {
            int a = input.nextInt();
            sir[i] = a;
        }
        int rezerva = 0;
        for (int i = 0; i < sir.length; i++) {
            for (int j = 0; j <= i; j++) {
                if (sir[i] < sir[j]) {
                    rezerva = sir[j];
                    sir[j] = sir[i];
                    sir[i] = rezerva;
                }
            }
        }
        int count = 0;
        int[] duplicate = new int[10];
        for (int i = 1; i < sir.length - 1; i++) {
            if ((sir[i] == sir[i - 1]) && (sir[i] != sir[i + 1])) {
                duplicate[count] = sir[i];
                count++;
            }
        }
        if (sir[sir.length - 1] == sir[sir.length - 2]) {
            duplicate[count] = sir[sir.length - 1];
        }
        for (int i = 0; i < duplicate.length; i++) {
            if (duplicate[i] != 0) {
                System.out.println(duplicate[i]);
            }
        }
    }

    public static void increasing() {
        Scanner input = new Scanner(System.in);
        int[] sir = new int[10];
        for (int i = 0; i <= 9; i++) {
            int a = input.nextInt();
            sir[i] = a;
        }
        int count = 1;
        int[] max = new int[10];
        max[0] = count;
        for (int i = 1; i < sir.length; i++) {
            if (sir[i] > sir[i - 1]) {
                count++;
            } else {
                count = 0;
            }
            max[i] = count;
        }
        int maxim = 0;
        for (int i = 0; i < max.length; i++) {
            if (max[i] > maxim) {
                maxim = max[i];
            }
        }
        System.out.println("In sirul de numere exista un subsir de " + maxim + " numere crescatoare");
    }

    public static void sneeze() {
        Scanner input = new Scanner(System.in);
        System.out.println("Spune ceva:");
        String text = input.nextLine();
        if (text.toLowerCase().startsWith("acho")) {
            System.out.println("Sanatate! Ti-ai facut vaccin?");
        } else {
            System.out.println("Esti sanatos. Du-te si invata Java.");
        }
    }

    public static void tooMuch() {
        Random numar = new Random();
        System.out.println("Spune tu:");
        int nr1 = numar.nextInt(100);
        System.out.println(nr1);
        Scanner input = new Scanner(System.in);
        System.out.println("Spun eu:");
        int nr2 = input.nextInt();
        while (nr1 != nr2) {
            if (nr2 > nr1) {
                System.out.println("Prea mare");
                nr2 = input.nextInt();
            } else if (nr2 < nr1) {
                System.out.println("Prea mic");
                nr2 = input.nextInt();
            }
        }
        if (nr1 == nr2) {
            System.out.println("Exact!");
        }
    }

    public static void reverse (String s) {
        String[] sir = s.split(" ", 0);
        for (int i=sir.length-1; i>=0; i--) {
            System.out.print(sir[i] + " ");
        }
    }
}

