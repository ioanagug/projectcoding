import java.sql.SQLOutput;
import java.util.Scanner;

public class Application {
    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);
        System.out.println("Introduceti numarul problemei:");
        int task = keyboard.nextInt();
        switch (task) {
            case 1: {
                System.out.println("Dati diametrul cercului pentru a calcula perimetrul:");
                double diam = keyboard.nextDouble();
                diamPerim(diam);
                break;
            }
            case 2: {
                System.out.println("Introduceti valoarea greutatii dvs. in kg");
                float weight = keyboard.nextFloat();
                System.out.println("Introduceti valoarea inaltimii dvs. in cm");
                int height = keyboard.nextInt();
                bodyMassIndex(weight, height);
                break;
            }
            case 3: {
                quadraticEq(7, 5, -6);
                break;
            }
            case 4: {
                fizzBuzz(21);
                break;
            }
            case 5: {
                System.out.println("Introduceti un numar:");
                int numar = keyboard.nextInt();
                System.out.println("Numerele prime sunt:");
                nrPrime(numar);
                break;
            }
            case 6: {
                harmonic(3);
                break;
            }
            case 7: {
                fibonacci(10);
                break;
            }
            case 8: {
                System.out.println("Introduceti cele doua numere:");
                float a = keyboard.nextFloat();
                float b = keyboard.nextFloat();
                calculator(a, b);
                break;
            }
            case 9: {
                wave(50);
                break;
            }
            case 10: {
                System.out.println("Intrduceti numarul dorit:");
                int nr = keyboard.nextInt();
                patratPerfect(nr);
            }
        }


    }

    public static void diamPerim(double diam) {
        double pi = Math.PI;
        double perim = pi * diam;
        System.out.println("Perimetrul cercului este " + perim);
    }

    public static void bodyMassIndex(float weight, int height) {
        float h = (float) height / 100;
        float bmi = weight / (h * h);
        if ((bmi <= 24.9) && (bmi >= 18.5)) {
            System.out.println("Your BMI is optimal, BMI = " + bmi);
        } else {
            System.out.println("Your BMI is not optimal, BMI = " + bmi);
        }
    }

    public static void quadraticEq(int a, int b, int c) {
        int delta = b * b - 4 * a * c;
        float x1, x2;
        x1 = (float) ((-b - Math.sqrt(delta)) / (2 * a));
        x2 = (float) ((-b + Math.sqrt(delta)) / (2 * a));
        if (delta > 0) {
            System.out.println("Solutiile ecuatiei sunt " + x1 + " si " + x2);
        } else {
            System.out.println("Delta este negativ, ecuatie imposibila");
        }
    }

    public static void fizzBuzz(int a) {
        for (int i = 1; i <= a; i++) {
            if (i % 3 == 0) {
                if (i % 7 == 0) {
                    System.out.println("FizzBuzz");
                } else {
                    System.out.println("Fizz");
                }
            } else if (i % 7 == 0) {
                System.out.println("Buzz");
            } else {
                System.out.println(i);
            }
        }
    }

    public static boolean prim(int a) {
        boolean prim = true;
        for (int i = 2; i < a; i++) {
            if (a % i == 0) {
                prim = false;
            }
        }
        return prim;
    }

    public static void nrPrime(int a) {
        for (int i = 2; i < a; i++) {
            if (prim(i)) {
                System.out.print(i + " ");
            }
        }
    }

    public static void harmonic(int a) {
        float h = 1.0f;
        float raport = 1;
        for (int i = 2; i <= a; i++) {
            raport = (float) 1 / i;
            h = (float) h + raport;
        }
        System.out.println("Valoarea harmonicii pentru " + a + " este " + h);
    }

    public static void fibonacci(int a) {
        int i = 0;
        int j = 1;
        int k = i + j;
        System.out.print(i + " " + j + " " + k + " ");
        int count = 3;
        while (count < a) {
            i = j;
            j = k;
            k = i + j;
            count = count + 1;
            System.out.print(k + " ");
        }
        System.out.println();
        System.out.println("Al " + a + "-lea numar Fibonacci este " + k);
    }

    public static void calculator(float a, float b) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Introduceti operatia dorita:");
        String semn = keyboard.next();
        float rezultat = 0;
        switch (semn) {
            case "+": {
                rezultat = a + b;
                System.out.println("Suma numerelor este " + rezultat);
                break;
            }
            case "-": {
                rezultat = a - b;
                System.out.println("Diferenta numerelor este " + rezultat);
                break;
            }
            case "*": {
                rezultat = a * b;
                System.out.println("Inmultirea numerelor este " + rezultat);
                break;
            }
            case "/": {
                if (b != 0) {
                    rezultat = a / b;
                    System.out.println("Inmultirea numerelor este " + rezultat);
                } else {
                    System.out.println("Rezultat imposibil");
                }
                break;
            }
            default: {
                System.out.println("Nu se cunoaste aceasta operatie");
            }
        }
    }

    public static void wave(int sfarsit) {
        int modulo1 = 2;
        int modulo2 = 7;
        for (int i=1; i<= sfarsit; i++){
            if ((i%8==0)||(i%8==1)) {
                System.out.print("*");
            } else {
                System.out.print(" ");
            }
        }
        System.out.println();
        for (int j = 1; j <= 3; j++) {
            for (int i = 1; i <= sfarsit; i++) {
                if ((i % 8 == modulo1) || (i % 8 == modulo2)) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            modulo1 = modulo1 + 1;
            modulo2 = modulo2 - 1;
            System.out.println();
        }
    }

    public static void patratPerfect (int n) {
        int count =0;
        for (int i=1; i<=n; i++) {
            if (i*i==n) {
                count=1;
                System.out.println(n + " este patratul perfect al lui " + i);
            }
        }
    if (count==0) {
        System.out.println("Numarul nu este un patrat perfect");
    }
    }

}







