public class ExercitiiLeet {
    public static void main(String[] args) {
        int[] height = {1, 8, 6, 2, 5, 4, 8, 3, 7};
//        System.out.println(maxArea(height));
//        isValid("({}]");

        String text = "a";
        //      System.out.println("Ultimul cuvant are " + lengthOfLastWord(text) + " litere");
        //     char x = '9';
        //      System.out.println((int) x);
        //System.out.println(isPalindrome(text));
        String text1 = "bcAB";
//        System.out.println(isAnagram1(text, text1));
        int[] sir={0,0,0,0};
 //       System.out.println(findMiddleIndex(sir));
        System.out.println(numJewelsInStones(text,text1));
    }

    public static int maxArea(int[] height) {
        int area = 0;
        int maxArea = 0;
        for (int i = 0; i < height.length; i++) {
            for (int j = 0; j <= i; j++) {
                if (i != j) {
                    if (height[i] <= height[j]) {
                        area = height[i] * Math.abs(j - i);
                        if (area > maxArea) {
                            maxArea = area;
                        }
                    } else {
                        area = height[j] * Math.abs(j - i);
                        if (area > maxArea) {
                            maxArea = area;
                        }
                    }
                }
            }
        }
        return maxArea;
    }

    public static boolean isValid(String s) {
        boolean valid = true;
        char[] deschise = {'(', '[', '{'};
        char[] inchise = {')', ']', '}'};

        for (int i = 0; i <= s.length() - 1; i++) {
            if (s.charAt(i) == deschise[0]) {
                if ((s.charAt(i + 1) == inchise[1]) || (s.charAt(i + 1) == inchise[2])) {
                    valid = false;
                }
            } else if (s.charAt(i) == deschise[1]) {
                if ((s.charAt(i + 1) == inchise[0]) || (s.charAt(i + 1) == inchise[2])) {
                    valid = false;
                }
            } else if (s.charAt(i) == deschise[2]) {
                if ((s.charAt(i + 1) == inchise[0]) || (s.charAt(i + 1) == inchise[1])) {
                    valid = false;
                }
            }
        }
        if (s.length() % 2 != 0) {
            valid = false;
        }
//            int par1=0,par2=0,dr1=0,dr1=0,aco1=0,aco2=0;
//            for (int i=0; i<s.length(); i++) {
//                switch (s.charAt(i)) {
//                    case deschise[0] :{
//                        par1++;
//                        break;
//                    }
//                    case inchise[0] : {
//                        par2++;
//                        break;
//                    }
//                }
//            }
        System.out.println(valid);
        return valid;
    }

    public static int lengthOfLastWord(String s) {
        int length;
        String[] sir = s.split(" ", 0);
        String cuvant = sir[sir.length - 1];
        length = cuvant.length();
        return length;
    }

    public static boolean isPalindrome(String s) {
        boolean palindrom;
        String[] sir = new String[s.length()];
        s = s.toLowerCase();
        String cuvant = "";
        for (int i = 0; i < s.length(); i++) {
            if (((s.charAt(i) >= 97) && (s.charAt(i) <= 122)) || ((s.charAt(i) >= 48) && (s.charAt(i) <= 57))) {
                cuvant = cuvant + s.valueOf(s.charAt(i));
            }
        }
        if ((cuvant.length() <= 100000) && (cuvant.length() >= 1)) {
            int lungime = cuvant.length() - 1;
            String invers = cuvant.valueOf(cuvant.charAt(lungime));
            for (int i = lungime - 1; i >= 0; i--) {
                invers = invers.toLowerCase() + cuvant.charAt(i);
            }
            if (cuvant.equals(invers)) {
                palindrom = true;
                return palindrom;
            } else {
                palindrom = false;
                return palindrom;
            }
        } else if (cuvant.equals("")) {
            return true;
        } else return false;
    }

    public static boolean isAnagram(String s, String t) {
        boolean anagram = true;
        char[] sir1 = new char[s.length()];
        char[] sir2 = new char[t.length()];
        if (s.length() != t.length()) {
            anagram = false;
        } else if ((s.length() == 1) && (t.length() == 1)) {
            if ((s.charAt(0) == t.charAt(0))) {
                anagram = true;
            } else {
                anagram = false;
            }
        } else {
            for (int i = 0; i < s.length() - 1; i++) {
                sir1[i] = s.charAt(i);
                sir2[i] = t.charAt(i);
            }
        }
        char rezerva = '0';
        for (int i = 0; i < sir1.length; i++) {
            for (int j = 0; j <= i; j++) {
                if (sir1[j] > sir1[i]) {
                    rezerva = sir1[j];
                    sir1[j] = sir1[i];
                    sir1[i] = rezerva;
                }
            }
        }
        rezerva = '0';
        for (int i = 0; i < sir2.length; i++) {
            for (int j = 0; j <= i; j++) {
                if (sir2[j] > sir2[i]) {
                    rezerva = sir2[j];
                    sir2[j] = sir2[i];
                    sir2[i] = rezerva;
                }
            }
        }
        if (s.length() == t.length()) {
            for (int i = 0; i < s.length(); i++) {
                if (sir1[i] != sir2[i]) {
                    anagram = false;
                }
            }
        }
        return anagram;
    }

    public static boolean isAnagram1(String s, String t) {
        boolean anagram = true; //char 0-127
        int[] vectorFrecS = new int[128];
        int[] vectorFrecT = new int[128];
        for (int i = 0; i < s.length(); i++) {
            vectorFrecS[s.charAt(i)]++;
            vectorFrecT[t.charAt(i)]++;
        }
        if (s.length() != t.length()) {
            anagram = false;
        } else {
            for (int i = 0; i < vectorFrecS.length - 1; i++) {
                if (vectorFrecS[i] != vectorFrecT[i]) {
                    anagram = false;
                }
            }
        }
        return anagram;
    }
/* 2,3,-1,8,4
suma stg =  2+3+(-1)
 */
    public static int findMiddleIndex(int[] nums) {
        int middleInd=-1;
        int[] sumaStg = new int[nums.length];
        int[] sumaDr = new int[nums.length];
        sumaStg[0]=0;
        sumaDr[0]=0;
        for (int i = 1; i < nums.length; i++) {
            sumaStg[i]=sumaStg[i-1]+nums[i-1];
            }
        for (int i = nums.length-2; i >=0; i--) {
            sumaDr[i]=sumaDr[i+1]+nums[i+1];
        }
        for (int i=0; i<sumaStg.length; i++) {
            if (sumaStg[i]==sumaDr[i]) {
                middleInd=i;
                break;
            }
        }
        return middleInd;
    }

    public static int numJewelsInStones(String jewels, String stones) {
        int nrJewel = 0;
        for (int i=0; i<stones.length(); i++) {
            for (int j=0; j<jewels.length(); j++) {
                if (stones.charAt(i)==jewels.charAt(j)) {
                    nrJewel++;
                }
            }
        }
        return nrJewel;
    }
}

